package ch.welld.exercise.pattrec.plane;

import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.List;
import java.util.random.RandomGenerator;
import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import static ch.welld.exercise.pattrec.util.Numbers.roundIfNeeded;
import static ch.welld.exercise.pattrec.util.Numbers.toBigDecimal;

import ch.welld.exercise.pattrec.plane.impl.PlaneImpl;

public final class PlaneTests {
  private static final RandomGenerator random = RandomGenerator.getDefault();

  private static final double MAX_RANDOM_DOUBLE = 100.0;
  private static final double MIN_RANDOM_DOUBLE = -MAX_RANDOM_DOUBLE;

  @Test
  public void badN0Test() {
    final Plane plane = new PlaneImpl();
    assertThrows(IllegalArgumentException.class, () -> plane.findLinesContainingNPoints(0));
  }

  @Test
  public void badN1Test() {
    final Plane plane = new PlaneImpl();
    assertThrows(IllegalArgumentException.class, () -> plane.findLinesContainingNPoints(1));
  }

  @Test
  public void noPointsTest() {
    final Plane plane = new PlaneImpl();
    assertTrue(plane.points().isEmpty());
    assertTrue(plane.findLinesContainingNPoints(2).isEmpty());
    assertTrue(plane.findLinesContainingNPoints(3).isEmpty());
  }

  @Test
  public void onePointTest() {
    final Plane plane = new PlaneImpl();
    final PlanePoint pp = randomPoint();
    plane.newPoint(pp);
    assertEquals(plane.points().size(), 1);
    assertEquals(plane.points(), Collections.singleton(pp));
    assertTrue(plane.findLinesContainingNPoints(2).isEmpty());
    assertTrue(plane.findLinesContainingNPoints(3).isEmpty());
  }

  @Test
  public void twoPointsTest() {
    final Plane plane = new PlaneImpl();
    final PlanePoint pp1 = randomPoint(), pp2 = randomPoint();
    plane.newPoint(pp1);
    plane.newPoint(pp2);

    assertEquals(plane.points().size(), 2);
    assertEquals(plane.points(), Set.of(pp1, pp2));

    final Set<PlaneLine> lines = plane.findLinesContainingNPoints(2);
    assertEquals(lines.size(), 1);
    final PlaneLine line = lines.iterator().next();
    assertEquals(line.points().size(), 2);
    assertEquals(line.points(), Set.of(pp1, pp2));

    assertTrue(plane.findLinesContainingNPoints(3).isEmpty());
  }

  @Test
  public void clearTest() {
    final Plane plane = new PlaneImpl();
    plane.newPoint(randomPoint());
    assertEquals(plane.points().size(), 1);
    plane.clear();
    assertEquals(plane.points().size(), 0);
  }

  @Test
  public void everyPointsCopyIsDistinctTest() {
    final Plane plane = new PlaneImpl();
    plane.newPoint(randomPoint());
    assertFalse(plane.points() == plane.points());
    assertEquals(plane.points(), plane.points());
  }

  @Test
  public void squareTest() {
    final Plane plane = new PlaneImpl();
    final BigDecimal delta = new BigDecimal(10);
    final PlanePoint pp1 = randomPoint();
    final PlanePoint pp2 = pp1.up(delta);
    final PlanePoint pp3 = pp2.right(delta);
    final PlanePoint pp4 = pp3.down(delta);
    final PlanePoint pp5 = pp4.left(delta);

    List.of(pp1, pp2, pp3, pp4, pp5).forEach(plane::newPoint);

    assertEquals(plane.points().size(), 4);
    assertEquals(plane.points(), Set.of(pp1, pp2, pp3, pp4));

    final Set<PlaneLine> lines = plane.findLinesContainingNPoints(2);
    assertEquals(lines.size(), 6);
    assertTrue(lines.stream().map(PlaneLine::points).mapToInt(Set::size).allMatch(sz -> sz == 2));

    assertTrue(plane.findLinesContainingNPoints(3).isEmpty());
  }

  @Test
  public void threeAndThreeVerticalPointsTest() {
    final Plane plane = new PlaneImpl();
    final BigDecimal delta = new BigDecimal(10);
    final PlanePoint pp1 = randomPoint();
    final PlanePoint pp2 = pp1.up(delta);
    final PlanePoint pp3 = pp2.up(delta);
    final PlanePoint pp4 = pp3.right(delta);
    final PlanePoint pp5 = pp4.up(delta);
    final PlanePoint pp6 = pp5.up(delta);

    List.of(pp1, pp2, pp3, pp4, pp5, pp6).forEach(plane::newPoint);

    assertEquals(plane.points().size(), 6);
    assertEquals(plane.points(), Set.of(pp1, pp2, pp3, pp4, pp5, pp6));

    final Set<PlaneLine> lines2 = plane.findLinesContainingNPoints(2);
    assertEquals(lines2.size(), 11);
    assertTrue(lines2.stream().map(PlaneLine::points).mapToInt(Set::size).allMatch(sz -> sz == 2 || sz == 3));

    final Set<PlaneLine> lines3 = plane.findLinesContainingNPoints(3);
    assertEquals(lines3.size(), 2);

    assertTrue(plane.findLinesContainingNPoints(4).isEmpty());
  }

  @Test
  public void threeAndThreeHorizontalPointsTest() {
    final Plane plane = new PlaneImpl();
    final BigDecimal delta = new BigDecimal(10);
    final PlanePoint pp1 = randomPoint();
    final PlanePoint pp2 = pp1.right(delta);
    final PlanePoint pp3 = pp2.right(delta);
    final PlanePoint pp4 = pp3.up(delta);
    final PlanePoint pp5 = pp4.right(delta);
    final PlanePoint pp6 = pp5.right(delta);

    List.of(pp1, pp2, pp3, pp4, pp5, pp6).forEach(plane::newPoint);

    assertEquals(plane.points().size(), 6);
    assertEquals(plane.points(), Set.of(pp1, pp2, pp3, pp4, pp5, pp6));

    final Set<PlaneLine> lines2 = plane.findLinesContainingNPoints(2);
    assertEquals(lines2.size(), 11);
    assertTrue(lines2.stream().map(PlaneLine::points).mapToInt(Set::size).allMatch(sz -> sz == 2 || sz == 3));

    final Set<PlaneLine> lines3 = plane.findLinesContainingNPoints(3);
    assertEquals(lines3.size(), 2);

    assertTrue(plane.findLinesContainingNPoints(4).isEmpty());
  }

  @Test
  public void specific1Test() {
    final Plane plane = new PlaneImpl();
    final PlanePoint pp1 = PlanePoint.builder().setX(-2).setY(-2).build();
    final PlanePoint pp2 = PlanePoint.builder().setX(-2).setY(3).build();
    final PlanePoint pp3 = PlanePoint.builder().setX(-2).setY(5).build();
    final PlanePoint pp4 = PlanePoint.builder().setX(4).setY(2).build();
    final PlanePoint pp5 = PlanePoint.builder().setX(4).setY(5).build();
    final PlanePoint pp6 = PlanePoint.builder().setX(6).setY(-2).build();
    final PlanePoint pp7 = PlanePoint.builder().setX(7).setY(4).build();
    final PlanePoint pp8 = PlanePoint.builder().setX(12).setY(-2).build();

    List.of(pp1, pp2, pp3, pp4, pp5, pp6, pp7, pp8).forEach(plane::newPoint);

    assertEquals(plane.points().size(), 8);
    assertEquals(plane.points(), Set.of(pp1, pp2, pp3, pp4, pp5, pp6, pp7, pp8));

    final Set<PlaneLine> lines2 = plane.findLinesContainingNPoints(2);
    assertEquals(lines2.size(), 20);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp1::equals).count(), 4);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp2::equals).count(), 5);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp3::equals).count(), 4);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp4::equals).count(), 2);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp5::equals).count(), 3);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp6::equals).count(), 1);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp7::equals).count(), 1);
    assertEquals(lines2.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp8::equals).count(), 0);
    assertTrue(lines2.stream().map(PlaneLine::points).mapToInt(Set::size).allMatch(sz -> sz == 2 || sz == 3));
    assertEquals(lines2.stream().map(PlaneLine::points).mapToInt(Set::size).filter(sz -> sz == 2).count(), 16);
    assertEquals(lines2.stream().map(PlaneLine::points).mapToInt(Set::size).filter(sz -> sz == 3).count(), 4);

    final Set<PlaneLine> lines3 = plane.findLinesContainingNPoints(3);
    assertEquals(lines3.size(), 4);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp1::equals).count(), 3);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp2::equals).count(), 0);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp3::equals).count(), 1);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp4::equals).count(), 0);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp5::equals).count(), 0);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp6::equals).count(), 0);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp7::equals).count(), 0);
    assertEquals(lines3.stream().map(PlaneLine::points).map(SortedSet::first).filter(pp8::equals).count(), 0);
    assertTrue(lines3.stream().map(PlaneLine::points).mapToInt(Set::size).allMatch(sz -> sz == 3));

    assertTrue(plane.findLinesContainingNPoints(4).isEmpty());
  }

  private PlanePoint randomPoint() {
    final double x = random.nextDouble(MIN_RANDOM_DOUBLE, MAX_RANDOM_DOUBLE);
    final double y = random.nextDouble(MIN_RANDOM_DOUBLE, MAX_RANDOM_DOUBLE);

    return PlanePoint.builder().setX(x).setY(y).build();
  }
}
