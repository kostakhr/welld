package ch.welld.exercise.pattrec.plane;

import java.util.random.RandomGenerator;
import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static ch.welld.exercise.pattrec.util.Numbers.roundIfNeeded;
import static ch.welld.exercise.pattrec.util.Numbers.toBigDecimal;

public final class PlanePointTests {
  private static final RandomGenerator random = RandomGenerator.getDefault();

  private static final double MAX_RANDOM_DOUBLE = 100.0;
  private static final double MIN_RANDOM_DOUBLE = -MAX_RANDOM_DOUBLE;

  @Test
  public void zeroTest() {
    final PlanePoint pp = PlanePoint.builder().setX(BigDecimal.ZERO).setY(BigDecimal.ZERO).build();
    assertEquals(pp.x(), BigDecimal.ZERO);
    assertEquals(pp.y(), BigDecimal.ZERO);
  }

  @Test
  public void oneTest() {
    final PlanePoint pp = PlanePoint.builder().setX(BigDecimal.ONE).setY(BigDecimal.ONE).build();
    assertEquals(pp.x(), BigDecimal.ONE);
    assertEquals(pp.y(), BigDecimal.ONE);
  }

  @Test
  public void randomTest() {
    final double x = random.nextDouble(MIN_RANDOM_DOUBLE, MAX_RANDOM_DOUBLE);
    final double y = random.nextDouble(MIN_RANDOM_DOUBLE, MAX_RANDOM_DOUBLE);

    final PlanePoint pp = PlanePoint.builder().setX(x).setY(y).build();

    assertEquals(pp.x().compareTo(roundIfNeeded(toBigDecimal(x), PlanePoint.SCALE)), 0);
    assertEquals(pp.y().compareTo(roundIfNeeded(toBigDecimal(y), PlanePoint.SCALE)), 0);
  }

  @Test
  public void oneHundredRandomTests() {
    for (int i = 0; i < 10000; ++i) randomTest();
  }
}
