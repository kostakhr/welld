package ch.welld.exercise.pattrec.plane;

import java.util.random.RandomGenerator;
import java.math.BigDecimal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static ch.welld.exercise.pattrec.util.Numbers.roundIfNeeded;
import static ch.welld.exercise.pattrec.util.Numbers.toBigDecimal;

public final class LineCoefficientsTests {
  private static final Logger log = LogManager.getLogger(LineCoefficientsTests.class);
  private static final RandomGenerator random = RandomGenerator.getDefault();

  private static final double MIN_RANDOM_DOUBLE = 1.0;
  private static final double MAX_RANDOM_DOUBLE = 100.0;

  @Test
  public void sameXTest() {
    final PlanePoint p1 = PlanePoint.builder().setX(BigDecimal.ZERO).setY(BigDecimal.ZERO).build();
    final PlanePoint p2 = PlanePoint.builder().setX(BigDecimal.ZERO).setY(BigDecimal.ONE).build();
    assertThrows(IllegalArgumentException.class, () -> LineCoefficients.from(p1, p2));
  }

  @Test
  public void sameYTest() {
    final PlanePoint p1 = PlanePoint.builder().setX(BigDecimal.ZERO).setY(BigDecimal.ZERO).build();
    final PlanePoint p2 = PlanePoint.builder().setX(BigDecimal.ONE).setY(BigDecimal.ZERO).build();
    assertEquals(LineCoefficients.from(p1, p2).k().compareTo(BigDecimal.ZERO), 0);
  }

  @Test
  public void slope45Test() {
    final PlanePoint p1 = PlanePoint.builder().setX(BigDecimal.ZERO).setY(BigDecimal.ZERO).build();
    final PlanePoint p2 = PlanePoint.builder().setX(BigDecimal.ONE).setY(BigDecimal.ONE).build();
    assertEquals(LineCoefficients.from(p1, p2).k().compareTo(BigDecimal.ONE), 0);
  }

  @Test
  public void randomSlope45Test() {
    final BigDecimal x = roundIfNeeded(toBigDecimal(random.nextDouble(MIN_RANDOM_DOUBLE, MAX_RANDOM_DOUBLE)), PlanePoint.SCALE);
    final BigDecimal y = roundIfNeeded(toBigDecimal(random.nextDouble(MIN_RANDOM_DOUBLE, MAX_RANDOM_DOUBLE)), PlanePoint.SCALE);

    final PlanePoint p1 = PlanePoint.builder().setX(x).setY(y).build();
    final PlanePoint p2 = PlanePoint.builder().setX(x.add(x)).setY(y.add(x)).build();
    assertEquals(LineCoefficients.from(p1, p2).k().compareTo(BigDecimal.ONE), 0);
  }

  @Test
  public void sameCoefficientsTest() {
    final PlanePoint p1 = PlanePoint.builder().setX(-2).setY(-2).build();
    final PlanePoint p2 = PlanePoint.builder().setX(4).setY(2).build();
    final PlanePoint p3 = PlanePoint.builder().setX(7).setY(4).build();

    final LineCoefficients lc12 = LineCoefficients.from(p1, p2);
    final LineCoefficients lc13 = LineCoefficients.from(p1, p3);
    final LineCoefficients lc23 = LineCoefficients.from(p2, p3);

    assertEquals(lc12, lc13);
    assertEquals(lc12, lc23);
    assertEquals(lc13, lc23);
  }
}
