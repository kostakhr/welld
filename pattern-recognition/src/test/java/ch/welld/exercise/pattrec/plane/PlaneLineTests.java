package ch.welld.exercise.pattrec.plane;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public final class PlaneLineTests {
  @Test
  public void onePointTest() {
    final PlanePoint p1 = PlanePoint.builder().setX(BigDecimal.ZERO).setY(BigDecimal.ZERO).build();
    assertThrows(IllegalArgumentException.class, () -> PlaneLine.builder(p1).build());
  }

}
