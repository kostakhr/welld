package ch.welld.exercise.pattrec.util;

import java.util.random.RandomGenerator;
import java.math.BigInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import static ch.welld.exercise.pattrec.util.Numbers.NonNegativeIntString;

public final class NumbersTests {
  private static final Logger log = LogManager.getLogger(NumbersTests.class);
  private static final RandomGenerator random = RandomGenerator.getDefault();

  private void testAdd(final int i1, final int i2) {
    final int sum = i1 + i2;
    final NonNegativeIntString num1 = new NonNegativeIntString(i1);
    final NonNegativeIntString num2 = new NonNegativeIntString(i2);
    log.debug("i1 = {}, i2 = {}, sum = {}, num1 = {}, num2 = {}", i1, i2, sum, num1, num2);
    final String sumAsString = num1.add(num2);
    assertEquals(sum, Integer.parseInt(sumAsString));
    assertEquals(String.valueOf(sum), sumAsString);
  }

  private void testAdd(final String i1, final String i2) {
    final BigInteger sum = new BigInteger(i1).add(new BigInteger(i2));
    final NonNegativeIntString num1 = new NonNegativeIntString(i1);
    final NonNegativeIntString num2 = new NonNegativeIntString(i2);
    log.debug("i1 = {}, i2 = {}, sum = {}, num1 = {}, num2 = {}", i1, i2, sum, num1, num2);
    final String sumAsString = num1.add(num2);
    assertEquals(sum, new BigInteger(sumAsString));
    assertEquals(String.valueOf(sum), sumAsString);
  }

  private void testAdd(final String i1, final int left1, final int right1, final int expectedValue1, final String i2, final int left2, final int right2, final int expectedValue2) {
    final NonNegativeIntString num1 = new NonNegativeIntString(i1, left1, right1);
    final NonNegativeIntString num2 = new NonNegativeIntString(i2, left2, right2);
    final int value1 = Integer.parseInt(num1.toString());
    final int value2 = Integer.parseInt(num1.toString());
    final int sum = value1 + value2;
    log.debug("i1 = {}, left1 = {}, right1 = {}, value1 = {}, expectedValue1 = {}, i2 = {}, left2 = {}, right2 = {}, value2 = {}, expectedValue2 = {}, sum = {}, num1 = {}, num2 = {}",
        i1, left1, right1, value1, expectedValue1, i2, left2, right2, value2, expectedValue2, sum, num1, num2);
    assertEquals(value1, expectedValue1);
    assertEquals(value2, expectedValue2);
    final String sumAsString = num1.add(num2);
    assertEquals(sum, Integer.valueOf(sumAsString));
    assertEquals(String.valueOf(sum), sumAsString);
  }

  private void testSubtract(final int i1, final int i2) {
    final int diff = i1 - i2;
    final NonNegativeIntString num1 = new NonNegativeIntString(i1);
    final NonNegativeIntString num2 = new NonNegativeIntString(i2);
    log.debug("i1 = {}, i2 = {}, diff = {}, num1 = {}, num2 = {}", i1, i2, diff, num1, num2);
    final String diffAsString = num1.subtract(num2);
    assertEquals(diff, Integer.parseInt(diffAsString));
    assertEquals(String.valueOf(diff), diffAsString);
  }

  private void testMultiply(final int i1, final int i2) {
    final int product = i1 * i2;
    final NonNegativeIntString num1 = new NonNegativeIntString(i1);
    final NonNegativeIntString num2 = new NonNegativeIntString(i2);
    log.debug("i1 = {}, i2 = {}, product = {}, num1 = {}, num2 = {}", i1, i2, product, num1, num2);
    final String productAsString = num1.multiply(num2);
    assertEquals(product, Integer.parseInt(productAsString));
    assertEquals(String.valueOf(product), productAsString);
  }

  private void testMultiply(final String i1, final String i2) {
    final BigInteger product = new BigInteger(i1).multiply(new BigInteger(i2));
    final NonNegativeIntString num1 = new NonNegativeIntString(i1);
    final NonNegativeIntString num2 = new NonNegativeIntString(i2);
    log.debug("i1 = {}, i2 = {}, product = {}, num1 = {}, num2 = {}", i1, i2, product, num1, num2);
    final String productAsString = num1.multiply(num2);
    assertEquals(product, new BigInteger(productAsString));
    assertEquals(String.valueOf(product), productAsString);
  }

  @Test
  public void zeroAddTest() {
    testAdd(0, 0);
  }

  @Test
  public void zeroAndOneAddTest() {
    testAdd(0, 1);
    testAdd(1, 0);
  }

  @Test
  public void fixedOnesAddTest() {
    testAdd(1, 11);
    testAdd(11, 1);
    testAdd(0, 11);
    testAdd(11, 0);
    testAdd(1, 10);
    testAdd(10, 1);
    testAdd(0, 10);
    testAdd(10, 0);
  }

  @Test
  public void randomAddTest() {
    final int i1 = random.nextInt(0, 100000000);
    final int i2 = random.nextInt(0, 100000000);
    testAdd(i1, i2);
  }

  @Test
  public void cycle10000AddTest() {
    for (int i1 = 0; i1 < 100; ++i1) {
      for (int i2 = 0; i2 < 100; ++i2) {
        testAdd(i1, i2);
      }
    }
  }

  @Test
  public void stringAddTest() {
    testAdd("0", "0");
    testAdd("1", "0");
    testAdd("0", "1");
    testAdd("1", "1");
    testAdd("00", "00");
    testAdd("01", "00");
    testAdd("00", "01");
    testAdd("01", "01");
  }

  @Test
  public void bigStringAddTest() {
    testAdd("12934719264573281093747126357812936418091823749020", "098720934879123094672365192736417237401230");
    testAdd("0", "00000098720934879123094672365192736417237401230");
    testAdd("00", "00000098720934879123094672365192736417237401230");
    testAdd("12934719264573281093747126357812936418091823749020", "0");
    testAdd("12934719264573281093747126357812936418091823749020", "00");
    testAdd("12934719264573281093747126357812936418091823749020", "12934719264573281093747126357812936418091823749020");
  }

  @Test
  public void substringAddTest() {
    testAdd("0000", 3, 1, 0, "0000", 4, 2, 0);
    testAdd("0010", 3, 1, 1, "0100", 4, 2, 1);
  }

  @Test
  public void cycle100SubtractTest() {
    for (int i1 = 10; i1 >= 0; --i1) {
      for (int i2 = i1; i2 >= 0; --i2) {
        testSubtract(i1, i2);
      }
    }
  }

  @Test
  public void someSubtractsTest() {
    testSubtract(1000, 100);
    testSubtract(999, 99);

    final int i1 = random.nextInt(1, 10000);
    final int i2 = random.nextInt(0, i1);
    testSubtract(i1, i2);
  }

  @Test
  public void testOneAndZero() {
    assertTrue(new NonNegativeIntString(0).isZero());
    assertTrue(new NonNegativeIntString(1).isOne());
    assertTrue(new NonNegativeIntString("0").isZero());
    assertTrue(new NonNegativeIntString("1").isOne());
    assertTrue(new NonNegativeIntString("2002", 3, 1).isZero());
    assertTrue(new NonNegativeIntString("2012", 3, 1).isOne());
  }

  @Test
  public void zeroMultiplyTest() {
    testMultiply(0, 0);
    testMultiply(0, random.nextInt(1, 10000));
    testMultiply(random.nextInt(1, 10000), 0);
  }

  @Test
  public void oneMultiplyTest() {
    testMultiply(1, 0);
    testMultiply(0, 1);
    testMultiply(1, 1);
    testMultiply(1, random.nextInt(1, 10000));
    testMultiply(random.nextInt(1, 10000), 1);
  }

  @Test
  public void oneDigitMultiplyTest() {
    final int i1 = random.nextInt(100, 1000);
    for (int i2 = 0; i2 < 10; ++i2) testMultiply(i1, i2);
  }

  @Test
  public void twoDigitsMultiplyTest() {
    final int i1 = random.nextInt(100, 1000);
    for (int i2 = 0; i2 < 100000; ++i2) testMultiply(i1, i2);
  }

  @Test
  public void bigStringMultiplyTest() {
    testMultiply("12934719264573281093747126357812936418091823749020", "098720934879123094672365192736417237401230");
    testMultiply("0", "00000098720934879123094672365192736417237401230");
    testMultiply("00", "00000098720934879123094672365192736417237401230");
    testMultiply("12934719264573281093747126357812936418091823749020", "0");
    testMultiply("12934719264573281093747126357812936418091823749020", "00");
    testMultiply("1293471926457328109374712635781293641809182374902012934719264573281093747126357812936418091823749020", "1293471926457328109374712635781293641809182374902012934719264573281093747126357812936418091823749020");
  }

}
