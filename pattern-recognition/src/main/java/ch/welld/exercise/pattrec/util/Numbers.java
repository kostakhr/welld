package ch.welld.exercise.pattrec.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import static java.math.RoundingMode.HALF_UP;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public final class Numbers {
  private Numbers() {}
  private static final Logger log = LogManager.getLogger(Numbers.class);

  public static BigDecimal roundIfNeeded(final BigDecimal value, final int scale) {
    if (value == null) return null;
    if (value.scale() < scale) return value;
    return value.setScale(scale, HALF_UP).stripTrailingZeros();
  }

  public static BigDecimal toBigDecimal(final Double value) {
    if (value == null) return null;
    return BigDecimal.valueOf(value);
  }

  /* A possibly very big non-negative integer represented as a string of decimal digits.
   * Each number comes as a reversed positional range in a given string (right inclusive, left exclusive).
   * Its exact substring can be obtained as num.substring(num.length() - left, num.length() - right)
   *
   * For example, given a string "21", left = 2 and right = 0 the number to consider is 21, 
   * given a string "51037", left = 4 and right = 2 the number to consider is 10.
   *
   * If a specific range results in an empty string or fully/partially goes beyond num's length the number to consider is 0.
   * It's supposed that left and right indices always come non-negative. No explicit check is made.
   */
  public static final class NonNegativeIntString {
    private final String num;
    private final int left, right, fullLength, length;
    private final boolean isEmpty, isZero;

    public NonNegativeIntString(final int num) {
      this(String.valueOf(num));
    }

    public NonNegativeIntString(final BigInteger num) {
      this(num.toString());
    }

    public NonNegativeIntString(final String num) {
      this(num, num.length(), 0);
    }

    public NonNegativeIntString(final String num, final int _left, final int right) {
      this.num = num;
      this.fullLength = num.length();

      // align left and right indices to the actual substring length
      this.right = Integer.min(right, this.fullLength);
      int left = Integer.max(Integer.min(_left, this.fullLength), this.right);
      while (left > this.right && this.num.charAt(this.fullLength - left) == '0') --left; // in case a substring starts from a couple of '0' they're skipped
      this.left = left;

      this.isEmpty = this.fullLength == 0 || this.right == this.fullLength || this.left == this.right;
      this.length = isEmpty ? 0 : this.left - this.right;
      this.isZero = isEmpty || this.length == 1 && num.charAt(rightPositionStringIndex()) == '0';

      /*
      log.debug("num = {}, _left = {}, right = {}, this.fullLength = {}, this.left = {}, this.right = {}, this.length = {}, this.isEmpty = {}, this.isZero = {}, ",
          num, _left, right, this.fullLength, this.left, this.right, this.length, this.isEmpty, this.isZero);
      */
    }

    public boolean isZero() { return isZero; }

    public boolean isOne() {
      final boolean result = isEmpty == false && length == 1 && num.charAt(rightPositionStringIndex()) == '1';
      // log.debug("is empty = {}, length = {}, rightPositionStringIndex() = {}, num.charAt(rightPositionStringIndex()) = {}, isone = {}", isEmpty, length, rightPositionStringIndex(), num.charAt(rightPositionStringIndex()), result);
      return result;
    }

    public String toString() {
      return this.isZero ? "0" : this.num.substring(this.fullLength - this.left, this.fullLength - this.right);
    }

    private String toPaddedString(int pow10) {
      if (this.isZero) return "0";
      if (pow10 == 0) return toString();
      final int resultLength = length + pow10;
      final char[] result = new char[resultLength];

      for (int resultIndex = resultLength; pow10 > 0; --pow10) result[--resultIndex] = '0';
      num.getChars(this.fullLength - this.left, this.fullLength - this.right, result, 0);
      return new String(result);
    }

    private int rightPositionStringIndex() {
      return this.fullLength - this.right - 1;
    }

    private static int charToInt(final char c) {
      return c - '0';
    }

    private int intAt(final int index) {
      return charToInt(this.num.charAt(index));
    }

    private static char toChar(final int digit) {
      return (char) (digit + '0');
    }

    // cached positional sum results
    private static final class OnePositionSumResult {
      final int addend;
      final char resultDigit;

      private OnePositionSumResult(final int sum) {
        if (sum >= 10) {
          resultDigit = toChar(sum - 10);
          addend = 1;
        }
        else {
          resultDigit = toChar(sum);
          addend = 0;
        }
      }

      private static final OnePositionSumResult[] bySum = new OnePositionSumResult[20];
      static {
        for (int i = 0, n = bySum.length; i < n; ++i) bySum[i] = new OnePositionSumResult(i);
      }

      static OnePositionSumResult bySum(final int sum) {
        return bySum[sum];
      }

      static final OnePositionSumResult BY_SUM_0 = bySum[0];
    }

    public String add(final NonNegativeIntString that) {
      return add(that, 0);
    }

    private String add(final NonNegativeIntString that, final int pow10) {
      if (this.isZero) return that.toPaddedString(pow10);
      if (that.isZero) this.toPaddedString(pow10);

      return this.length > that.length ? add(this, that, pow10) : add(that, this, pow10);
    }

    private static String add(final NonNegativeIntString longer, final NonNegativeIntString shorter, int pow10) {
      // allocating the result, it may have one position more than the longer int in two cases:
      // 1) the most significant position of the longer int is equal to 9 and accumulated addend is 1
      // 2) both ints have the same length and their most significant positions produce sum >= 10
      // also it's extended accordingly if some zeros are to be added
      final int resultLength = longer.length + 1 + pow10;
      final char[] result = new char[resultLength];

      int longerIndex = longer.rightPositionStringIndex();
      int resultIndex = resultLength;
      for (; pow10 > 0; --pow10) result[--resultIndex] = '0';
      OnePositionSumResult onePositionSumResult = OnePositionSumResult.BY_SUM_0;

      // processing sum of the shorter int and the corresponding part of the longer int
      for (int shorterIndex = shorter.rightPositionStringIndex(), shorterN = shorter.fullLength - shorter.left; shorterIndex >= shorterN;) {
        onePositionSumResult = OnePositionSumResult.bySum(shorter.intAt(shorterIndex--) + longer.intAt(longerIndex--) + onePositionSumResult.addend);
        result[--resultIndex] = onePositionSumResult.resultDigit;
      }

      final int longerN = longer.fullLength - longer.left;

      // processing eventually present addend propagating it to the longer int part right after the shorter
      while (longerIndex >= longerN && onePositionSumResult.addend == 1) {
        onePositionSumResult = OnePositionSumResult.bySum(longer.intAt(longerIndex--) + onePositionSumResult.addend);
        result[--resultIndex] = onePositionSumResult.resultDigit;
      }

      if (onePositionSumResult.addend == 1) {
        // the last sum requires setting of the most significant position
        result[--resultIndex] = '1';
      }
      else {
        // a couple of most significant positions of the longer int require to be copied as is to the result
        while (longerIndex >= longerN) result[--resultIndex] = longer.num.charAt(longerIndex--);
      }

      return new String(result, resultIndex, resultLength - resultIndex);
    }

    // cached positional subtract results
    private static final class OnePositionDifference {
      private static final int OFFSET = 10;

      final int addend;
      final char resultDigit;

      private OnePositionDifference(final int diff) {
        if (diff < 0) {
          resultDigit = toChar(OFFSET + diff);
          addend = -1;
        }
        else {
          resultDigit = toChar(diff);
          addend = 0;
        }
      }

      private static final OnePositionDifference[] byDiff = new OnePositionDifference[20];
      static {
        for (int i = -OFFSET; i < OFFSET; ++i) byDiff[i + OFFSET] = new OnePositionDifference(i);
      }

      static OnePositionDifference byDiff(final int diff) {
        return byDiff[diff + OFFSET];
      }

      static final OnePositionDifference BY_DIFF_0 = byDiff[OFFSET];
    }

    // works only if this is greater or equal than that
    public String subtract(final NonNegativeIntString that) {
      return subtract(that, 0);
    }

    private String subtract(final NonNegativeIntString that, final int pow10) {
      if (that.isZero) return this.toPaddedString(pow10);
      if (this.isZero || this.length < that.length) throw new IllegalArgumentException("this (" + this + ") is lower than that (" + that + ") for subtraction");

      return subtract(this, that, pow10);
    }

    private static String subtract(final NonNegativeIntString longer, final NonNegativeIntString shorter, int pow10) {
      final int resultLength = longer.length + pow10;
      final char[] result = new char[resultLength];

      int longerIndex = longer.rightPositionStringIndex();
      int resultIndex = resultLength;
      for (; pow10 > 0; --pow10) result[--resultIndex] = '0';
      OnePositionDifference onePositionDifference = OnePositionDifference.BY_DIFF_0;

      // processing diff of the shorter int and the corresponding part of the longer int
      for (int shorterIndex = shorter.rightPositionStringIndex(), shorterN = shorter.fullLength - shorter.left; shorterIndex >= shorterN;) {
        onePositionDifference = OnePositionDifference.byDiff(-shorter.intAt(shorterIndex--) + longer.intAt(longerIndex--) + onePositionDifference.addend);
        result[--resultIndex] = onePositionDifference.resultDigit;
      }

      final int longerN = longer.fullLength - longer.left;

      // processing eventually present negative addend propagating it to the longer int part right after the shorter
      while (longerIndex >= longerN && onePositionDifference.addend == -1) {
        onePositionDifference = onePositionDifference.byDiff(longer.intAt(longerIndex--) + onePositionDifference.addend);
        result[--resultIndex] = onePositionDifference.resultDigit;
      }

      if (onePositionDifference.addend == -1) {
        // means that that is bigger than this (negative result cannot be produced)
        throw new IllegalArgumentException("this (" + longer + ") is lower than that (" + shorter + ") for subtraction");
      }
      else {
        // a couple of most significant positions of the longer int require to be copied as is to the result
        if (longerIndex >= longerN) do { result[--resultIndex] = longer.num.charAt(longerIndex--); } while (longerIndex >= longerN);
        else {
          // some zeros might become lead, they're going to be reduced
          for (;; ++resultIndex) {
            if (resultIndex == resultLength) return "0";
            if (result[resultIndex] != '0') break;
          }
        }
      }

      return new String(result, resultIndex, resultLength - resultIndex);
    }

    public String multiply(final NonNegativeIntString that) {
      return multiply(that, 0);
    }

    private String multiply(final NonNegativeIntString that, final int pow10) {
      if (this.isZero || that.isZero) return "0";
      if (this.isOne()) return that.toPaddedString(pow10);
      if (that.isOne()) return this.toPaddedString(pow10);

      return this.length > that.length ? multiply(this, that, pow10) : multiply(that, this, pow10);
    }

    // if shorter is of one digit then it proceed with a simple cycle
    // otherwise it applies karatsuba method with half length = longer.length / 2
    // shorter = pow(10, half length) * a + b
    // longer  = pow(10, half length) * c + d
    private static String multiply(final NonNegativeIntString longer, final NonNegativeIntString shorter, final int pow10) {
      if (shorter.length == 1) {
        final String result = multiply(longer, shorter.intAt(shorter.rightPositionStringIndex()), pow10);
        // log.debug("multiply({}, {}, {}) - shorter is of one digit, result = {}", longer, shorter, pow10, result);
        return result;
      }

      final int halfLength = longer.length / 2 + longer.length % 2;

      final int middleShorterIndex = shorter.right + halfLength;
      final int middleLongerIndex  = longer.right  + halfLength;

      final NonNegativeIntString a = new NonNegativeIntString(shorter.num, shorter.left, middleShorterIndex);
      final NonNegativeIntString b = new NonNegativeIntString(shorter.num, middleShorterIndex, shorter.right);

      final NonNegativeIntString c = new NonNegativeIntString(longer.num, longer.left, middleLongerIndex);
      final NonNegativeIntString d = new NonNegativeIntString(longer.num, middleLongerIndex, longer.right);

      final NonNegativeIntString bdMult10PowerHalfLength = new NonNegativeIntString(b.multiply(d, halfLength));
      final NonNegativeIntString bd = new NonNegativeIntString(bdMult10PowerHalfLength.num, bdMult10PowerHalfLength.left, bdMult10PowerHalfLength.right + halfLength);

      // log.debug("multiply({}, {}, {})\na = {}\nb = {}\nc = {}\nd = {}\nbdMult10PowerHalfLength = {}\nbd = {}", longer, shorter, pow10, a, b, c, d, bdMult10PowerHalfLength, bd);

      // basic optimization
      if (a.isZero()) {
        final String result = bd.add(new NonNegativeIntString(b.multiply(c, halfLength)), pow10);
        // log.debug("multiply({}, {}, {}) - a is zero, result = {}", longer, shorter, pow10, result);
        return result;
      }

      final NonNegativeIntString acMult10PowerDoubleHalfLength = new NonNegativeIntString(a.multiply(c, 2 * halfLength));

      // basic optimizations
      if (b.isZero()) {
        if (d.isZero()) return acMult10PowerDoubleHalfLength.toPaddedString(pow10);
        final NonNegativeIntString adMult10PowerHalfLength = new NonNegativeIntString(a.multiply(d, halfLength));
        return acMult10PowerDoubleHalfLength.add(adMult10PowerHalfLength, pow10);
      }

      if (d.isZero()) {
        final NonNegativeIntString bcMult10PowerHalfLength = new NonNegativeIntString(b.multiply(c, halfLength));
        return acMult10PowerDoubleHalfLength.add(bcMult10PowerHalfLength, pow10);
      }

      final NonNegativeIntString acMult10PowerHalfLength = new NonNegativeIntString(acMult10PowerDoubleHalfLength.num, acMult10PowerDoubleHalfLength.left, acMult10PowerDoubleHalfLength.right + halfLength);

      final NonNegativeIntString aPlusB = new NonNegativeIntString(a.add(b));
      final NonNegativeIntString cPlusD = new NonNegativeIntString(c.add(d));
      final NonNegativeIntString aPlusBmultCPlusDmult10PowerHalfLength = new NonNegativeIntString(aPlusB.multiply(cPlusD, halfLength));

      final NonNegativeIntString minuend = new NonNegativeIntString(new NonNegativeIntString(acMult10PowerDoubleHalfLength.add(bd)).add(aPlusBmultCPlusDmult10PowerHalfLength));
      final NonNegativeIntString subtrahend = new NonNegativeIntString(acMult10PowerHalfLength.add(bdMult10PowerHalfLength));
      final String result = minuend.subtract(subtrahend, pow10);

      /*
      log.debug("multiply({}, {}, {})\nacMult10PowerDoubleHalfLength = {}\nacMult10PowerHalfLength = {}\naPlusB = {}\ncPlusD = {}\naPlusBmultCPlusDmult10PowerHalfLength = {}\nminuend = {}\nsubtrahend = {}\nresult = {}",
          longer, shorter, pow10, acMult10PowerDoubleHalfLength, acMult10PowerHalfLength, aPlusB, cPlusD, aPlusBmultCPlusDmult10PowerHalfLength, minuend, subtrahend, result);
      */

      return result;
    }

    private static String multiply(final NonNegativeIntString longer, final int oneDigit, int pow10) {
      // allocating the result, it may have one position more than the longer int when
      // the product of the most significant position of the longer int and one digit is >= 10
      // also it's extended accordingly if some zeros are to be added
      final int resultLength = longer.length + 1 + pow10;
      final char[] result = new char[resultLength];

      int resultIndex = resultLength;
      for (; pow10 > 0; --pow10) result[--resultIndex] = '0';

      int addend = 0;
      for (int longerIndex = longer.rightPositionStringIndex(), longerN = longer.fullLength - longer.left; longerIndex >= longerN;) {
        final int product = longer.intAt(longerIndex--) * oneDigit + addend;
        if (product >= 10) {
          result[--resultIndex] = toChar(product % 10);
          addend = product / 10;
        }
        else {
          result[--resultIndex] = toChar(product);
          addend = 0;
        }
      }
      if (addend > 0) result[--resultIndex] = toChar(addend);
      return new String(result, resultIndex, resultLength - resultIndex);
    }
  }
}