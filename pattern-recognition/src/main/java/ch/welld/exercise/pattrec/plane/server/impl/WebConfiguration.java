package ch.welld.exercise.pattrec.plane.server.impl;

import java.util.List;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import com.google.gson.GsonBuilder;

import ch.welld.exercise.pattrec.plane.PlanePoint;
import ch.welld.exercise.pattrec.plane.PlaneLine;

@Configuration
public class WebConfiguration extends WebMvcConfigurationSupport {

  @Override
  protected void configureMessageConverters(final List<HttpMessageConverter<?>> converters) {
    converters.add(jsonConverter());
    addDefaultHttpMessageConverters(converters);
  }

  private HttpMessageConverter jsonConverter() {
    final GsonHttpMessageConverter result = new GsonHttpMessageConverter();

    result.setGson(new GsonBuilder()
        .registerTypeAdapter(PlanePoint.class, new PlanePointSerializer())
        .registerTypeAdapter(PlaneLine.class, new PlaneLineSerializer())
        .create()
    );

    return result;
  }
}
