package ch.welld.exercise.pattrec.plane.server.impl;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonDeserializationContext;

import ch.welld.exercise.pattrec.plane.PlanePoint;

public final class PlanePointSerializer implements JsonSerializer<PlanePoint>, JsonDeserializer<PlanePoint> {
  private static final String X = "x";
  private static final String Y = "y";

  @Override
  public PlanePoint deserialize(final JsonElement jsonElement, final Type type, final JsonDeserializationContext jsonDeserializationContext) {
    final JsonObject jsonObject = jsonElement.getAsJsonObject();
    return PlanePoint.builder().setX(jsonObject.get(X).getAsBigDecimal()).setY(jsonObject.get(Y).getAsBigDecimal()).build();
  }

  @Override
  public JsonElement serialize(final PlanePoint pp, final Type type, final JsonSerializationContext jsonSerializationContext) {
    return serialize(pp);
  }

  static JsonElement serialize(final PlanePoint pp) {
    final JsonObject result = new JsonObject();
    result.addProperty(X, pp.x());
    result.addProperty(Y, pp.y());
    return result;
  }
}