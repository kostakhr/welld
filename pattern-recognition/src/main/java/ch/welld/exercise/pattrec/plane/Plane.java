package ch.welld.exercise.pattrec.plane;

import java.util.Set;
import java.util.NavigableSet;

/** basic definition of minimalistic plane contract */
public interface Plane {
  
  /** adds a new point
   * @param point a new point to add
   * @return true if point is really new, false if it was already defined on a plane
   * @throws NullPointerException if point is null
   */
  boolean newPoint(PlanePoint point);

  /** clears all the plane */
  void clear();

  /** gets all points
   * @return new immutable copy of existing points
   */
  NavigableSet<PlanePoint> points();

  /** finds lines with at least n points
   * @param n a minimal number of points that every line in result must contain
   * @return set of lines
   * @throws IllegalArgumentException if n < 2
   */
  Set<PlaneLine> findLinesContainingNPoints(int n);
}
