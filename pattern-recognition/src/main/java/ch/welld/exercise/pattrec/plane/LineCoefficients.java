package ch.welld.exercise.pattrec.plane;

import java.math.BigDecimal;
import java.util.Comparator;

import static java.util.Objects.requireNonNull;

import org.springframework.util.Assert;

import static java.math.RoundingMode.HALF_UP;

public final class LineCoefficients {
  public static final int SCALE = PlanePoint.SCALE * 10; // empirically should be enough for coeff approximations
  private static final String TO_STRING_FORMAT = String.format("(%%.%1$df, %%.%1$df)", SCALE);
  private static final String MANDATORY_POINTS_REQUIREMENT = "line coefficients point is mandatory";

  private final BigDecimal k, m;
  private final int hashCode;

  private LineCoefficients(final BigDecimal k, final BigDecimal m) {
    this.k = k;
    this.m = m;
    this.hashCode = 31 * k.hashCode() + m.hashCode();
  }

  public static LineCoefficients from(final PlanePoint p1, final PlanePoint p2) {
    Assert.isTrue(requireNonNull(p1, MANDATORY_POINTS_REQUIREMENT).x().compareTo(requireNonNull(p2, MANDATORY_POINTS_REQUIREMENT).x()) != 0, "points must have different x");
    final BigDecimal dividend = p1.y().subtract(p2.y());
    final BigDecimal divisor  = p1.x().subtract(p2.x());
    final BigDecimal k = dividend.divide(divisor, SCALE, HALF_UP);
    final BigDecimal m = p1.y().subtract(dividend.multiply(p1.x()).divide(divisor, SCALE, HALF_UP));
    return new LineCoefficients(k, m);
  }

  public BigDecimal k() { return k; }
  public BigDecimal m() { return m; }

  @Override
  public String toString() {
    return String.format(TO_STRING_FORMAT, k, m);
  }

  @Override
  public final boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null) return false;
    if (o instanceof LineCoefficients) {
      final LineCoefficients another = (LineCoefficients) o;
      return k.compareTo(another.k) == 0 && m.compareTo(another.m) == 0;
    }
    return false;
  }

  @Override
  public final int hashCode() {
    return hashCode;
  }
}
