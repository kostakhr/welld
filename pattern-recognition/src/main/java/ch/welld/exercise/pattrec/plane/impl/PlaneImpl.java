package ch.welld.exercise.pattrec.plane.impl;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;
import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.function.Function;
import java.util.stream.Stream;
import java.math.BigDecimal;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toSet;
import static java.util.stream.Collectors.collectingAndThen;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.util.Assert;

import ch.welld.exercise.pattrec.plane.Plane;
import ch.welld.exercise.pattrec.plane.PlaneLine;
import ch.welld.exercise.pattrec.plane.PlanePoint;
import ch.welld.exercise.pattrec.plane.LineCoefficients;

/** implementation with O(n^2) computational complexity and memory consumption of O(n^2) in extreme case when all the lines are of two points only */
public final class PlaneImpl implements Plane {
  private static final Logger log = LogManager.getLogger(PlaneImpl.class);

  private final NavigableSet<PlanePoint> points = new TreeSet<>(PlanePoint.ASCENDING_COMPARATOR);

  /** {@inheritDoc} */
  @Override
  public boolean newPoint(final PlanePoint point) {
    synchronized(points) {
      return points.add(requireNonNull(point, "plane point is mandatory"));
    }
  }

  /** {@inheritDoc} */
  @Override
  public void clear() {
    synchronized(points) {
      points.clear();
    }
  }

  /** {@inheritDoc} */
  @Override
  public NavigableSet<PlanePoint> points() {
    synchronized(points) {
      return Collections.unmodifiableNavigableSet(new TreeSet<>(points));
    }
  }

  /** {@inheritDoc} */
  @Override
  public Set<PlaneLine> findLinesContainingNPoints(final int n) {
    Assert.isTrue(n >= 2, "every line consists of at least two points");

    final Set<PlaneLine> result;
    synchronized(points) {
      result = findLinesContainingNPointsInternal(n);
    }

    log.trace("findLinesContainingNPoints({}) = \n{}", n, result);
    return result;
  }

  /** internal implementation of lines finder that require guaranty of exclusive access to points set */
  private Set<PlaneLine> findLinesContainingNPointsInternal(final int n) {
    // check some basic cases
    final int pointsCount = points.size();
    if (pointsCount < 2) return Collections.emptySet();
    if (pointsCount == 2) {
      if (n > 2) return Collections.emptySet();
      if (n == 2) {
        final Iterator<PlanePoint> i = points.iterator();
        return Collections.singleton(PlaneLine.builder(i.next()).nextPoint(i.next()).build());
      }
    }

    // points count >= 3, n >= 2

    final Map<LineCoefficients, PlaneLine.Builder> lines = new HashMap<>();
    final Map<BigDecimal, PlaneLine.Builder> verticalLines = new HashMap<>();

    for (final Iterator<PlanePoint> i1 = points.iterator(); i1.hasNext(); ) { // iterating left part of point pairs
      final PlanePoint p1 = i1.next();
      final BigDecimal currentX = p1.x();

      BigDecimal lastX = currentX; // track the last points that possibly have the same x

      for (final Iterator<PlanePoint> i2 = points.tailSet(p1, false).iterator(); i2.hasNext(); ) { // iterating right part of point pairs
        final PlanePoint p2 = i2.next();
        lastX = p2.x();

        if (currentX.compareTo(lastX) == 0) { // special case of point pair with the same x
          verticalLines.computeIfAbsent(currentX, key -> PlaneLine.builder(p1)).nextPoint(p2);
        }
        else { // p1 and p2 of different x
          lines.computeIfAbsent(LineCoefficients.from(p1, p2), key -> PlaneLine.builder(p1)).nextPoint(p2);
        }
      }

      // if p1 and the last point have the same x it can be stopped because all of them are on the same line and no other line exists at the end
      if (currentX.compareTo(lastX) == 0) break;
    }

    return Stream.of(lines.values(), verticalLines.values()).flatMap(Collection::stream)
        .filter(plb -> plb.size() >= n).map(PlaneLine.Builder::build)
        .collect(collectingAndThen(toSet(), Collections::unmodifiableSet));
  }

}
