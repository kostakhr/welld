package ch.welld.exercise.pattrec.util.web;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.support.XmlWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.eclipse.jetty.servlet.ServletHolder;

public final class DispatcherServletHolderFactoryBean implements FactoryBean<ServletHolder>, ApplicationContextAware, InitializingBean {
  private ApplicationContext applicationContext;
  @Override
  public void setApplicationContext(final ApplicationContext applicationContext) { this.applicationContext = applicationContext; }

  private ServletHolder instance;

  @Override
  public boolean isSingleton() { return true; }

  @Override
  public void afterPropertiesSet() { this.instance = createInstance(); }

  @Override
  public ServletHolder getObject() {
    if (instance == null) throw new IllegalStateException("Instance not initialized yet");
    return instance;
  }

  private String name, configLocation;
  private boolean throwExceptionIfNoHandlerFound = true;
  
  public void setName(final String name) { this.name = name; }
  public void setConfigLocation(final String configLocation) { this.configLocation = configLocation; }
  public void setThrowExceptionIfNoHandlerFound(final boolean throwExceptionIfNoHandlerFound) { this.throwExceptionIfNoHandlerFound = throwExceptionIfNoHandlerFound; }

  @Override
  public Class<ServletHolder> getObjectType() { return ServletHolder.class; }

  private ServletHolder createInstance() {
    final XmlWebApplicationContext ctx = new XmlWebApplicationContext();
    ctx.setParent(applicationContext);
    ctx.setConfigLocation(configLocation);
    final DispatcherServlet servlet = new DispatcherServlet(ctx);
    servlet.setThrowExceptionIfNoHandlerFound(throwExceptionIfNoHandlerFound);
    final ServletHolder holder = new ServletHolder(name, servlet);
    holder.setInitOrder(1);
    return holder;
  }
}
