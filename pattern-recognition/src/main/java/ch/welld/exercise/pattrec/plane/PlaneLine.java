package ch.welld.exercise.pattrec.plane;

import java.util.NavigableSet;
import java.util.TreeSet;
import java.util.Collections;

import static java.util.Objects.requireNonNull;

import org.springframework.util.Assert;

public final class PlaneLine {
  private final NavigableSet<PlanePoint> points;

  private PlaneLine(final NavigableSet<PlanePoint> points) {
    this.points = points;
  }

  public NavigableSet<PlanePoint> points() { return points; }

  @Override
  public String toString() {
    return points.toString();
  }

  @Override
  public final boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null) return false;
    if (o instanceof PlaneLine) {
      final PlaneLine another = (PlaneLine) o;
      return points.equals(another.points);
    }
    return false;
  }

  @Override
  public final int hashCode() {
    return points.hashCode();
  }

  public static Builder builder(final PlanePoint startingPoint) { return new Builder(startingPoint); }

  public static class Builder {
    private final NavigableSet<PlanePoint> points = new TreeSet<>(PlanePoint.ASCENDING_COMPARATOR);

    private Builder(final PlanePoint startingPoint) {
      points.add(requireNonNull(startingPoint, "every line must have a starting point"));
    }

    public Builder nextPoint(final PlanePoint nextPoint) {
      points.add(requireNonNull(nextPoint, "next point is mandatory for continuing a line"));
      return this;
    }

    public int size() { return points.size(); }

    public PlaneLine build() {
      Assert.isTrue(points.size() >= 2, "line must consist of at least 2 points");
      return new PlaneLine(Collections.unmodifiableNavigableSet(points));
    }
  }
}