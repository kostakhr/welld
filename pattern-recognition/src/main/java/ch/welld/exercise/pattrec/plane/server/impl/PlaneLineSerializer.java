package ch.welld.exercise.pattrec.plane.server.impl;

import java.util.NavigableSet;
import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonArray;
import com.google.gson.JsonSerializer;
import com.google.gson.JsonSerializationContext;

import ch.welld.exercise.pattrec.plane.PlaneLine;
import ch.welld.exercise.pattrec.plane.PlanePoint;

public final class PlaneLineSerializer implements JsonSerializer<PlaneLine> {
  @Override
  public JsonElement serialize(final PlaneLine pl, Type type, JsonSerializationContext jsonSerializationContext) {
    final NavigableSet<PlanePoint> points = pl.points();
    final JsonArray result = new JsonArray(points.size());
    points.stream().map(PlanePointSerializer::serialize).forEachOrdered(result::add);
    return result;
  }
}