package ch.welld.exercise.pattrec.plane;

import java.math.BigDecimal;
import java.util.Comparator;

import static java.util.Objects.requireNonNull;
import static ch.welld.exercise.pattrec.util.Numbers.toBigDecimal;
import static ch.welld.exercise.pattrec.util.Numbers.roundIfNeeded;

public final class PlanePoint {
  public static final Comparator<PlanePoint> ASCENDING_COMPARATOR = Comparator.comparing(PlanePoint::x).thenComparing(PlanePoint::y);

  public static final int SCALE = 2;
  private static final String TO_STRING_FORMAT = String.format("(%%.%1$df, %%.%1$df)", SCALE);

  private final BigDecimal x, y;
  private final int hashCode;

  private PlanePoint(final BigDecimal x, final BigDecimal y) {
    this.x = x;
    this.y = y;
    this.hashCode = 31 * x.hashCode() + y.hashCode();
  }

  public BigDecimal x() { return x; }
  public BigDecimal y() { return y; }

  public PlanePoint up(final BigDecimal delta) {
    return builder().setX(x).setY(y.add(requireNonNull(delta, "delta is mandatory"))).build();
  }
  public PlanePoint right(final BigDecimal delta) {
    return builder().setX(x.add(requireNonNull(delta, "delta is mandatory"))).setY(y).build();
  }
  public PlanePoint down(final BigDecimal delta) {
    return up(requireNonNull(delta.negate(), "delta is mandatory"));
  }
  public PlanePoint left(final BigDecimal delta) {
    return right(requireNonNull(delta.negate(), "delta is mandatory"));
  }

  @Override
  public String toString() {
    return String.format(TO_STRING_FORMAT, x, y);
  }

  @Override
  public final boolean equals(final Object o) {
    if (this == o) return true;
    if (o == null) return false;
    if (o instanceof PlanePoint) {
      final PlanePoint another = (PlanePoint) o;
      return x.compareTo(another.x) == 0 && y.compareTo(another.y) == 0;
    }
    return false;
  }

  @Override
  public final int hashCode() {
    return hashCode;
  }

  public static Builder builder() { return new Builder(); }

  public static class Builder {
    private BigDecimal x, y;

    public Builder setX(final BigDecimal x) {
      this.x = roundIfNeeded(x, SCALE);
      return this;
    }

    public Builder setX(final Double x) {
      return setX(toBigDecimal(x));
    }

    public Builder setX(final int x) {
      return setX(new BigDecimal(x));
    }

    public Builder setY(final BigDecimal y) {
      this.y = roundIfNeeded(y, SCALE);
      return this;
    }

    public Builder setY(final Double y) {
      return setY(toBigDecimal(y));
    }

    public Builder setY(final int y) {
      return setY(new BigDecimal(y));
    }

    public PlanePoint build() {
      return new PlanePoint(
        requireNonNull(x, "plane point X is mandatory"),
        requireNonNull(y, "plane point Y is mandatory")
      );
    }
  }
}