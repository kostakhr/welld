package ch.welld.exercise.pattrec.plane.server.impl;

import java.util.Set;
import java.util.NavigableSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.util.Assert;

import ch.welld.exercise.pattrec.plane.Plane;
import ch.welld.exercise.pattrec.plane.PlaneLine;
import ch.welld.exercise.pattrec.plane.PlanePoint;

@Controller
public final class PlaneHttpEntryPoints {
  private static final Logger log = LogManager.getLogger(PlaneHttpEntryPoints.class);

  @Autowired
  private Plane plane;

  @RequestMapping(value = "ping", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
  @ResponseBody
  public String ping() {
    log.trace("ping()");
    return "pong";
  }

  @RequestMapping(value = "point", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public void point(@RequestBody final PlanePoint point) {
    log.trace("point({})", point);
    plane.newPoint(point);
  }

  @RequestMapping(value = "space", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public NavigableSet<PlanePoint> getSpace() {
    log.trace("getSpace()");
    return plane.points();
  }

  @RequestMapping(value = "space", method = RequestMethod.DELETE)
  @ResponseBody
  public void deleteSpace() {
    log.trace("deleteSpace()");
    plane.clear();
  }

  @RequestMapping(value = "lines/{n}", method = RequestMethod.GET)
  @ResponseBody
  public Set<PlaneLine> lines(final @PathVariable int n) {
    log.trace("lines({})", n);
    return plane.findLinesContainingNPoints(n);
  }
}
