package ch.welld.exercise.pattrec.plane.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

/**
 * Main class that starts global Spring application context of pattern recognition demo HTTP server and waits for regular process interruption.
 */
public final class Main {
  private Main() {}
  private static final Logger log = LogManager.getLogger(Main.class);

  /**
   * Runner method. No args used.
   * @param args command line arguments - not used now
   */
  public static void main(final String[] args) throws Exception {
    final ClassPathXmlApplicationContext context;

    try {
      log.info("Starting Pattern Recognition Demo HTTP server");
      context = new ClassPathXmlApplicationContext(new String[] { "pattrec.xml" }, false);
      context.refresh();
      context.registerShutdownHook();
      log.info("Spring Application context successfully started");
    }
    catch (Exception e) {
      log.error("Failed to bring up application context: " + e.getMessage(), e);
      throw e;
    }

    log.info("Pattern Recognition Demo HTTP server is ready");
    waitForContextClosed(context);
  }

  private static void waitForContextClosed(final ClassPathXmlApplicationContext context) {
    while (context.isActive()) {
      try {
        Thread.sleep(10000L);
      }
      catch (InterruptedException e) {
        Thread.currentThread().interrupt();
      }
    }
  }
}