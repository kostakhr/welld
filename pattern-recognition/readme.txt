1) developed and tested with maven 3.8.5, gradle 7.4.2 and Oracle JDK 18.0.1

2) 'mvn clean test' or 'gradle clean build' produces a ready-to-run target with some unit tests passed

3) 'mvn exec:java' or 'gradle run' starts an http server listening by default to 0.0.0.0:9980 and waits for Ctrl-C to shut it down

4) src/main/resources/pattrec.properties can be modified to change listening address/port

5) curl-examples folder contains basic calls of existing endpoints with curl

6) package ch.welld.exercise.pattrec.util and its subpackages contain general-purpose utility classes

7) package ch.welld.exercise.pattrec.plane contains Plane definition (contract and data model)

8) package ch.welld.exercise.pattrec.plane.impl contains Plane implementation (O(n^2) complexity and O(n^2) memory consumption during calculation)

9) package ch.welld.exercise.pattrec.plane.server and its subpackages contain spring application starter and mvc facilities for endpoints handling and data conversion to and from json

10) spring application configuration is found in src/main/resources folder

NB: Plane point coordinates are scaled to two decimal points (supposed to be discretion/precision of measure)
