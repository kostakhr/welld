package ch.welld.exercise.pattrec.util

import java.math.BigDecimal
import java.math.RoundingMode

class Numbers private constructor() {
  companion object {
    val DEFAULT_ROUNDING_MODE = RoundingMode.HALF_UP

    @JvmStatic
    fun roundIfNeeded(value: BigDecimal?, scale: Int): BigDecimal? = if (value != null && value.scale() > scale) value.setScale(scale, DEFAULT_ROUNDING_MODE).stripTrailingZeros() else value

    @JvmStatic
    fun toBigDecimal(value: Double?): BigDecimal? = if (value != null) BigDecimal.valueOf(value) else null
  }
}