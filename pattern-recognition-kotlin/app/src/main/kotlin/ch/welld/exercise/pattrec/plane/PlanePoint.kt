package ch.welld.exercise.pattrec.plane

import java.math.BigDecimal

import ch.welld.exercise.pattrec.util.Numbers

class PlanePoint private constructor(val x: BigDecimal, val y: BigDecimal) {

  companion object {
    const val SCALE = 2

    private val TO_STRING_FORMAT = "(%%.%1\$df, %%.%1\$df)".format(SCALE);

    private val hashCode = 31 * PlanePoint::x.hashCode() + PlanePoint::y.hashCode()
  }

  override fun toString() = TO_STRING_FORMAT.format(x, y)

  override fun equals(other: Any?): Boolean = this === other || (other is PlanePoint) && this.x.compareTo(other.x) == 0 && this.y.compareTo(other.y) == 0

  override fun hashCode() = hashCode

  fun up(delta: BigDecimal): PlanePoint = Builder().x(x).y(y.add(delta)).build()
  fun right(delta: BigDecimal): PlanePoint = Builder().x(x.add(delta)).y(y).build()
  fun down(delta: BigDecimal): PlanePoint = up(delta.negate())
  fun left(delta: BigDecimal): PlanePoint = right(delta.negate())

  data class Builder(var x: BigDecimal? = null, var y: BigDecimal? = null) {
    fun x(x: BigDecimal?) = apply { this.x = Numbers.roundIfNeeded(x, SCALE) }
    fun x(x: Int) = x(BigDecimal(x))
    fun x(x: Double) = x(Numbers.toBigDecimal(x))

    fun y(y: BigDecimal?) = apply { this.y = Numbers.roundIfNeeded(y, SCALE) }
    fun y(y: Int) = y(BigDecimal(y))
    fun y(y: Double) = y(Numbers.toBigDecimal(y))

    fun build() = PlanePoint(x!!, y!!)
  }
}
