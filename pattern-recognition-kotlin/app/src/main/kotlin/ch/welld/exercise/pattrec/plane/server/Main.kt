package ch.welld.exercise.pattrec.plane.server

import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

class Main {
  val greeting: String
    get() {
      return "Hello World!"
    }
}

fun main() {
  logger.info { Main().greeting }
}
