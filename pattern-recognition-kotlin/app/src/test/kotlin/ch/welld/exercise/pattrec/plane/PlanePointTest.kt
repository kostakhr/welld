package ch.welld.exercise.pattrec.plane

import java.math.BigDecimal

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

import mu.KotlinLogging

private val logger = KotlinLogging.logger {}

class PlanePointTest {
  @Test fun plainPointBuilderMissingX() {
    assertFailsWith<NullPointerException>(
      message = "no exception found for missing x",
      block = { PlanePoint.Builder().y(0).build() }
    )
  }

  @Test fun plainPointBuilderMissingY() {
    assertFailsWith<NullPointerException>(
      message = "no exception found for missing y",
      block = { PlanePoint.Builder().x(0).build() }
    )
  }

  @Test fun plainPoint0() {
    val pp0_1 = PlanePoint.Builder().x(0).y(0).build()
    val pp0_2 = PlanePoint.Builder().x(BigDecimal("0.0")).y(BigDecimal("0.0")).build()
    assertEquals(pp0_1, pp0_2, "plane points 0 mismatch")
  }

  @Test fun plainPoint1() {
    val pp1_1 = PlanePoint.Builder().x(1).y(1).build()
    val pp1_2 = PlanePoint.Builder().x(BigDecimal.ONE).y(BigDecimal.ONE).build()
    assertEquals(pp1_1, pp1_2, "plane points 1 mismatch")
  }

  @Test fun plainPointFixedDouble() {
    val d1 = 43.5055
    val d2 = 32.4001
    val pp1 = PlanePoint.Builder().x(d1).y(d2).build()
    val pp2 = PlanePoint.Builder().x(BigDecimal.valueOf(d1)).y(BigDecimal.valueOf(d2)).build()
    assertEquals(pp1, pp2, "plane point constructed on fixed doubles mismatches the same constructed on big decimals")
  }

  @Test fun plainPointRandomDouble() {
    val dRange = -20000..20000
    val coeff = 0.01
    val d1 = dRange.random() * coeff
    val d2 = dRange.random() * coeff
    val pp1 = PlanePoint.Builder().x(d1).y(d2).build()
    val pp2 = PlanePoint.Builder().x(BigDecimal.valueOf(d1)).y(BigDecimal.valueOf(d2)).build()
    logger.info { "Checking if ${pp1} equals to ${pp2}" }
    assertEquals(pp1, pp2, "plane point constructed on random doubles mismatches the same constructed on big decimals")
  }
}
