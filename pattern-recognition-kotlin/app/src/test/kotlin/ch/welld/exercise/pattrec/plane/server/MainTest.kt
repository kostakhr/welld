package ch.welld.exercise.pattrec.plane.server

import kotlin.test.Test
import kotlin.test.assertNotNull

class MainTest {
  @Test fun appHasAGreeting() {
    val classUnderTest = Main()
    assertNotNull(classUnderTest.greeting, "app should have a greeting")
  }
}
