package ch.welld.exercise.pattrec.util

import java.math.BigDecimal

import kotlin.test.Test
import kotlin.test.assertTrue

class NumbersTest {
  @Test fun round0() {
    assertTrue(Numbers.roundIfNeeded(BigDecimal("0"), 2)!!.compareTo(BigDecimal.ZERO) == 0, "scaled 0 mismatch - 1")
    assertTrue(Numbers.roundIfNeeded(BigDecimal("0.0"), 2)!!.compareTo(BigDecimal.ZERO) == 0, "scaled 0 mismatch - 2")
    assertTrue(Numbers.roundIfNeeded(BigDecimal("0.0000000"), 2)!!.compareTo(BigDecimal.ZERO) == 0, "scaled 0 mismatch - 3")
    assertTrue(Numbers.roundIfNeeded(BigDecimal("0.00123"), 2)!!.compareTo(BigDecimal.ZERO) == 0, "scaled 0 mismatch - 4")
  }

  @Test fun round1() {
    assertTrue(Numbers.roundIfNeeded(BigDecimal("1"), 2)!!.compareTo(BigDecimal.ONE) == 0, "scaled 1 mismatch - 1")
    assertTrue(Numbers.roundIfNeeded(BigDecimal("1.0"), 2)!!.compareTo(BigDecimal.ONE) == 0, "scaled 1 mismatch - 2")
    assertTrue(Numbers.roundIfNeeded(BigDecimal("1.0000000"), 2)!!.compareTo(BigDecimal.ONE) == 0, "scaled 1 mismatch - 3")
    assertTrue(Numbers.roundIfNeeded(BigDecimal("1.00234"), 2)!!.compareTo(BigDecimal.ONE) == 0, "scaled 1 mismatch - 4")
  }
}
