CREATE TABLE product_recommendation(
  id integer,
  product_id1 integer NOT NULL,
  product_id2 integer NOT NULL,
  CONSTRAINT pk_product_recommendation PRIMARY KEY (id),
  CONSTRAINT u_product_ids UNIQUE (product_id1, product_id2) INCLUDE (id)
);


INSERT INTO product_recommendation(id, product_id1, product_id2)
SELECT i AS id, i AS product_id1, 1000001-i AS product_id2
  FROM generate_series(1, 1000000) s_id(i);

