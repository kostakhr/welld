package it.kk.playground.pgtemptabledemo;

import java.util.Set;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public final class Main {
  private Main() {}

  private static final int PRODUCTS_COUNT = 1000000;
  private static final int EXCLUDED_PRODUCTS_COUNT = 100000;

  private static Set<Integer> randomizeExcludedProductIds() {
  	final long startTimeMillis = System.currentTimeMillis();

  	final Random r = ThreadLocalRandom.current();
    final Set<Integer> result = new HashSet<>(EXCLUDED_PRODUCTS_COUNT);
    while (result.size() < EXCLUDED_PRODUCTS_COUNT) result.add(1 + r.nextInt(PRODUCTS_COUNT));

  	final long stopTimeMillis = System.currentTimeMillis();

    System.out.println(String.format("Generated %d product IDs to exclude from product recommendation... in %d milliseconds", EXCLUDED_PRODUCTS_COUNT, stopTimeMillis - startTimeMillis));

    return result;
  }

  private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
  private static final String DB_USER = "postgres";
  private static final String DB_PASSWORD = "postgres";

  private static Connection openConnection() throws SQLException {
    return DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
  }

  private static final String SQL_CREATE_EXCLUDED_PRODUCTS_TEMP_TABLE = "CREATE TEMP TABLE temp_excluded_products(id integer PRIMARY KEY) ON COMMIT DROP";
  private static final String SQL_INSERT_EXCLUDED_PRODUCT_ID = "INSERT INTO temp_excluded_products(id) VALUES(?)";

  private static void prepareExcludedProductsTempTable(final Connection conn, final Set<Integer> excludedProductIds) throws SQLException {
  	final long startTimeMillis = System.currentTimeMillis();

    try (final Statement st = conn.createStatement()) {
      st.execute(SQL_CREATE_EXCLUDED_PRODUCTS_TEMP_TABLE);
    }

    try (final PreparedStatement pst = conn.prepareStatement(SQL_INSERT_EXCLUDED_PRODUCT_ID)) {
      for (final Integer excludedProductId : excludedProductIds) {
        pst.setInt(1, excludedProductId);
        pst.addBatch();
      }
      pst.executeBatch();
    }

  	final long stopTimeMillis = System.currentTimeMillis();

    System.out.println(String.format("Prepared excluded products table... in %d milliseconds", stopTimeMillis - startTimeMillis));
  }

  private static final String SQL_SELECT_PRODUCT_RECOMMENDATIONS =
      "SELECT pr.id, pr.product_id1, pr.product_id2 FROM product_recommendation pr WHERE product_id1 NOT IN (SELECT id FROM temp_excluded_products)";

  private static void selectProductRecommendations(final Connection conn, final Set<Integer> excludedProductIds) throws SQLException {
  	final long startTimeMillis = System.currentTimeMillis();

  	int fetchedProductRecomendations = 0;

    try (final Statement st = conn.createStatement()) {
      try (final ResultSet rs = st.executeQuery(SQL_SELECT_PRODUCT_RECOMMENDATIONS)) {
        while (rs.next()) {
          ++fetchedProductRecomendations;
          final int productId1 = rs.getInt(2);
          if (excludedProductIds.contains(productId1)) {
            throw new IllegalStateException("An excluded product ID " + productId1 + " in unexpectedly present in fetched product recommendations");
          }
        }
      }
    }

  	final int expectedProductRecommendationsCount = PRODUCTS_COUNT - EXCLUDED_PRODUCTS_COUNT;
    if (fetchedProductRecomendations != expectedProductRecommendationsCount) {
      throw new IllegalStateException("Fetched " + fetchedProductRecomendations + " product recommendations while expected " + expectedProductRecommendationsCount);
    }

  	final long stopTimeMillis = System.currentTimeMillis();

    System.out.println(String.format("Selected %d product recommendations and asserted no excluded ID is present... in %d milliseconds", fetchedProductRecomendations, stopTimeMillis - startTimeMillis));
  }

  public static void main(final String[] args) throws SQLException {
    final Set<Integer> excludedProductIds = randomizeExcludedProductIds();

    try (final Connection conn = openConnection()) {
      conn.setAutoCommit(false);
      prepareExcludedProductsTempTable(conn, excludedProductIds);
      selectProductRecommendations(conn, excludedProductIds);
      conn.commit();
    }
  }
}
