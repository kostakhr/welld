CREATE TEMP TABLE temp_excluded_products(id integer PRIMARY KEY) ON COMMIT DROP;

INSERT INTO temp_excluded_products(id) VALUES(5);

SELECT pr.id, pr.product_id1, pr.product_id2
  FROM product_recommendation pr
 WHERE product_id1 NOT IN (SELECT id FROM temp_excluded_products);
